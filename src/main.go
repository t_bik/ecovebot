/*
 * This file is part of eCoveBoT.
 *
 * eCoveBoT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * eCoveBoT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with eCoveBoT.  If not, see <http://www.gnu.org/licenses/>.
 */

package main

import (
	"fmt"
	"log"
	"time"

	"github.com/anacrolix/torrent"
	"github.com/tucnak/telebot"
)

var (
	eTorrentClient     *torrent.Client
	authorizedAccounts = []string{
		"admin"
	}
)

type accounts struct {
	Name, prevSearch             string
	pageNToString, cleanString   string
	searchResult, selectedMagnet string
	isWaitingInput               bool
	pageSearch, tSelection       int
	tries                        int
	availableMagnets             []string

	torrentMagnet   *torrent.Torrent
	bytesDownloaded int64
	fileLength      int64
	fileDownloaded  bool

	archiveZip                 telebot.File
	archiveDoc                 telebot.Document
	archiveName                string
	archiveError, torrentError error
}

func checkErrors(err error) {
	if err != nil {
		log.Fatalln(err)
	}
}

func (a *accounts) checkCMD(msg telebot.Message, bot *telebot.Bot) {
	if a.isWaitingInput {
		a.cleanString = prepareString(msg.Text)
		a.startSearch(msg, bot)
	} else {
		switch {
		case msg.Text == "/hello":
			bot.SendMessage(msg.Chat, "Account Autorizzato!\nCiao, "+a.Name+"! ♡✧( ु•⌄• )", nil)
		case msg.Text == "/search":
			if a.selectedMagnet == "" {
				bot.SendMessage(msg.Chat, "Inserisci il nome del file da cercare", nil)
				a.isWaitingInput = true
			} else {
				bot.SendMessage(msg.Chat, "Hai già un download in corso.", nil)
			}
		case msg.Text == "/start":
			bot.SendMessage(msg.Chat, "eCoveBoT operativo e funzionante ✧٩(•́⌄•́๑)", nil)
		default:
			bot.SendMessage(msg.Chat, "Comando non riconosciuto", nil)
		}
	}
}

func main() {
	eCoveBot, err := telebot.NewBot("KEY")
	checkErrors(err)
	fmt.Println("[+] Bot online")

	eTorrentClient, err = torrent.NewClient(nil)
	defer eTorrentClient.Close()
	checkErrors(err)
	fmt.Println("[+] Client torrent avviato")

	messages := make(chan telebot.Message)
	eCoveBot.Listen(messages, 1*time.Second)
	getAccounts := make([]accounts, len(authorizedAccounts))

	for message := range messages {
		for index, checkAuthorization := range authorizedAccounts {
			if message.Sender.Username == checkAuthorization {
				if getAccounts[index].Name == "" {
					getAccounts[index] = accounts{
						Name:           message.Sender.Username,
						isWaitingInput: false,
						pageSearch:     0,
					}
					fmt.Println("* Account autorizzato: " + message.Sender.Username)
				}

				go getAccounts[index].checkCMD(message, eCoveBot)
				break
			}
		}
	}
}
