/*
 * This file is part of eCoveBoT.
 *
 * eCoveBoT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * eCoveBoT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with eCoveBoT.  If not, see <http://www.gnu.org/licenses/>.
 */

package main

import (
	"strconv"
	"strings"

	"github.com/PuerkitoBio/goquery"
	"github.com/tucnak/telebot"
)

func prepareString(arg0 string) string {
	trimSearchLeft := strings.TrimLeft(arg0, " ")
	trimSearch := strings.TrimRight(trimSearchLeft, " ")

	var cleanSearch string

	for index := 0; index < len(trimSearch); index++ {
		if string(trimSearch[index]) == " " && string(trimSearch[index+1]) != " " {
			cleanSearch += "+"
		} else {
			if string(trimSearch[index]) != " " {
				cleanSearch += strings.ToLower(string(trimSearch[index]))
			}
		}
	}

	return cleanSearch
}

func getElement(doc *goquery.Document, element string, search string, slice []string) []string {
	foundInfo := 0

	doc.Find(element).Each(func(i int, s *goquery.Selection) {
		getInfo := s.Find(search).Text()
		if getInfo != "" {
			slice[foundInfo] = getInfo
			foundInfo++
		}
	})

	return slice
}

func getMagnet(doc *goquery.Document, element string, search string, magnetIndex string, slice []string) []string {
	foundMagnet := 0
	var magnetURL string

	doc.Find(element).Each(func(i int, s *goquery.Selection) {
		aHref, _ := s.Attr(search)

		if strings.Index(aHref, magnetIndex) == 0 {
			for index := 0; index < len(aHref); index++ {
				if string(aHref[index]) != "&" {
					magnetURL += string(aHref[index])
				} else {
					slice[foundMagnet] = magnetURL
					magnetURL = ""
					foundMagnet++
					break
				}
			}
		}
	})

	return slice
}

func getTorrents(URL string, pagen string) ([]string, []string, []string) {
	composeURL := "censored :)"
	doc, err := goquery.NewDocument(composeURL)

	checkErrors(err)

	torrentsName := getElement(doc, "td", "a.detLink", make([]string, 30))
	magnetTorrents := getMagnet(doc, "a", "href", "magnet:?", make([]string, 30))
	infoTorrents := getElement(doc, "td", "font.detDesc", make([]string, 30))

	return torrentsName, magnetTorrents, infoTorrents
}

func (a *accounts) closeSearch() {
	a.prevSearch = ""
	a.cleanString = ""
	a.isWaitingInput = false
}

func (a *accounts) getInput(msg telebot.Message, bot *telebot.Bot) {
	if a.prevSearch != "" {
		a.cleanString = a.prevSearch
		if msg.Text == "/next" {
			a.pageSearch++
		} else if msg.Text == "/prev" && a.pageSearch > 0 {
			a.pageSearch--
		} else if msg.Text == "/close" {
			bot.SendMessage(msg.Chat, "Ricerca interrotta", nil)
			a.closeSearch()
		} else if _, err := strconv.Atoi(msg.Text); err == nil {
			a.tSelection, err = strconv.Atoi(msg.Text)
			checkErrors(err)

			if a.tSelection < 1 || a.tSelection > len(a.availableMagnets) {
				bot.SendMessage(msg.Chat, "Fuori range... (っ- ‸ – ς)", nil)
			} else {
				a.selectedMagnet = a.availableMagnets[a.tSelection-1]
				a.closeSearch()

				bot.SendMessage(msg.Chat, "Avvio il download...", nil)
				a.startDownload(msg, bot)
			}
		} else if strings.Index(msg.Text, "/") == 0 {
			bot.SendMessage(msg.Chat, "Comando non esistente. Ricerca interrotta.", nil)
			a.closeSearch()
		}
	}

}

func (a *accounts) startSearch(msg telebot.Message, bot *telebot.Bot) {

	a.getInput(msg, bot)

	if a.isWaitingInput && a.cleanString != "" {

		a.prevSearch = a.cleanString
		a.pageNToString = strconv.Itoa(a.pageSearch + 1)

		bot.SendMessage(msg.Chat, "Sto cercando... ヽ(〃･ω･)ﾉ", nil)

		tName, mName, tInfo := getTorrents(a.cleanString, a.pageNToString)
		a.availableMagnets = mName

		for index, value := range tName {
			indexToString := strconv.Itoa(index + 1)
			if value != "" {
				a.searchResult += "[" + indexToString + "] " + value + "\n# " + tInfo[index] + "\n\n"
			}
		}

		if a.searchResult != "" {
			bot.SendMessage(msg.Chat, "Pagina n°"+a.pageNToString+" selezionata\n\n"+a.searchResult+"\n\nSeleziona il file scelto con un numero", nil)

		} else {
			bot.SendMessage(msg.Chat, "Non ho trovato nulla... (っ- ‸ – ς)", nil)
			a.closeSearch()
		}

		a.searchResult = ""
	}
}
