/*
 * This file is part of eCoveBoT.
 *
 * eCoveBoT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * eCoveBoT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with eCoveBoT.  If not, see <http://www.gnu.org/licenses/>.
 */

package main

import (
	"fmt"
	"math/rand"
	"os"
	"time"

	"github.com/jhoonb/archivex"
	"github.com/tucnak/telebot"
)

func isFile(file string) bool {
	fileD, err := os.Open(file)
	checkErrors(err)
	defer fileD.Close()

	fileInfo, err := fileD.Stat()
	checkErrors(err)

	if fileInfo.Mode().IsDir() {
		return false
	}

	return true
}

func (a *accounts) createArchive(fileName string) {
	var archiveName string

	alphChars := "abcdefghilmnopqrstuvzABCDEFGHILMNOPQRSTUVZ"

	rand.Seed(time.Now().UTC().UnixNano())

	for index := 0; index < 5; index++ {
		archiveName += string(alphChars[rand.Intn(len(alphChars))])
	}

	zip := new(archivex.ZipFile)
	defer zip.Close()

	zip.Create(archiveName)

	if isFile(fileName) {
		zip.AddFile(fileName)
	} else {
		zip.AddAll(fileName, true)
	}

	a.archiveName = archiveName
}

func (a *accounts) startDownload(msg telebot.Message, bot *telebot.Bot) {

	a.torrentMagnet, a.torrentError = eTorrentClient.AddMagnet(a.selectedMagnet)
	checkErrors(a.torrentError)

	<-a.torrentMagnet.GotInfo()
	a.torrentMagnet.DownloadAll()

	bot.SendMessage(msg.Chat, "Ho iniziato il download di "+a.torrentMagnet.Info().Name, nil)

	for a.fileDownloaded = false; !a.fileDownloaded; {

		a.bytesDownloaded = a.torrentMagnet.BytesCompleted()
		a.fileLength = a.torrentMagnet.Info().TotalLength()

		if a.bytesDownloaded >= a.fileLength {
			bot.SendMessage(msg.Chat, "Download completato "+a.torrentMagnet.Info().Name, nil)

			bot.SendMessage(msg.Chat, "Creo ed invio il file... Potrebbe richiedere tempo", nil)

			a.createArchive(a.torrentMagnet.Info().Name)
			a.archiveZip, a.archiveError = telebot.NewFile("./" + a.archiveName + ".zip")
			fmt.Println("* Archivio invitato a " + a.Name)

			a.archiveDoc = telebot.Document{File: a.archiveZip}

			a.archiveError = bot.SendDocument(msg.Chat, &a.archiveDoc, nil)

			a.selectedMagnet = ""
			a.fileDownloaded = true

		}
	}
}
